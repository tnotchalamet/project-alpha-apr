from django.shortcuts import render, redirect, get_object_or_404
from companies.models import Company
from django.contrib.auth.decorators import login_required
from companies.forms import CompanyForm


@login_required
def CompanyListView(request):
    companies = Company.objects.all()
    context = {
        "companies": companies,
    }
    return render(request, "companies/companylist.html", context)


@login_required
def CompanyView(request, id):
    company = get_object_or_404(Company, id=id)
    context = {
        "company": company,
    }
    return render(request, "companies/company.html", context)


@login_required
def CreateCompany(request):
    if request.method == "POST":
        form = CompanyForm(request.POST)
        if form.is_valid():
            company = form.save(False)
            company.save()

        return redirect(CompanyListView)
    else:
        form = CompanyForm()
        context = {
            "form": form,
        }
        return render(request, "companies/createcompany.html", context)
