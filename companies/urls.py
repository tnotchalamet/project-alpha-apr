from django.urls import path
from companies.views import (
    CompanyView,
    CompanyListView,
    CreateCompany,
)


urlpatterns = [
    path("", CompanyListView, name="company_list"),
    path("<int:id>/", CompanyView, name="company"),
    path("create/", CreateCompany, name="create_company"),
]
