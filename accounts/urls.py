from django.urls import path
from accounts.views import UserLogin, UserLogout, UserSignup


urlpatterns = [
    path("login/", UserLogin, name="login"),
    path("logout/", UserLogout, name="logout"),
    path("signup/", UserSignup, name="signup"),
]
