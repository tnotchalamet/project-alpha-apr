from django.urls import path
from projects.views import ProjectListView, ProjectView, CreateProject

urlpatterns = [
    path("", ProjectListView, name="list_projects"),
    path("<int:id>/", ProjectView, name="show_project"),
    path("create/", CreateProject, name="create_project"),
]
