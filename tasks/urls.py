from django.urls import path
from tasks.views import CreateTask, TaskListView

urlpatterns = [
    path("create/", CreateTask, name="create_task"),
    path("mine/", TaskListView, name="show_my_tasks"),
]
