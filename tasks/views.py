from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from projects.views import ProjectListView


@login_required
def CreateTask(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()

        return redirect(ProjectListView)

    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/createtask.html", context)


@login_required
def TaskListView(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/tasks.html", context)
